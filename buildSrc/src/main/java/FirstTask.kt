import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class FirstTask: DefaultTask() {
    @TaskAction
    fun printHello(){
        print ("HELLO WORLD")
    }
}