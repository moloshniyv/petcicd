// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id ("com.android.application") version "7.1.1" apply false
    id ("com.android.library") version "7.1.1" apply false
    id ("org.jetbrains.kotlin.android") version "1.5.30" apply false
    id ("org.jlleitschuh.gradle.ktlint") version "10.3.0" apply false
}

buildscript {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        google()
    }
    dependencies {
        classpath("com.google.dagger:hilt-android-gradle-plugin:2.41")
        classpath("com.google.gms:google-services:4.3.10")
        classpath ("com.google.firebase:firebase-appdistribution-gradle:3.0.2")
    }
}

tasks.register<FirstTask>("firstTask")
//tasks.register<IncreaseBuildVersionTask>("increaseVersionNumber"){
//    version.set(findProperty("versionNumber") as String? )
//    file.set(file("./version.txt"))
//}
