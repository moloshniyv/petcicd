import com.google.firebase.appdistribution.gradle.AppDistributionExtension
apply(plugin = "com.google.gms.google-services")
apply(plugin = "com.google.firebase.appdistribution")

plugins {
  kotlin("kapt")
  id("com.android.application")
  id("org.jetbrains.kotlin.android")
  id("dagger.hilt.android.plugin")
  id("org.jlleitschuh.gradle.ktlint")
}

android {
  compileSdk = 32

  signingConfigs {
//    create("release") {
//      keyAlias = "releaseAlias"
//      keyPassword = "Qweqwe!234"
//      storeFile = file("../keystore/release.keystore")
//      storePassword = "Qweqwe!234"
//    }

    getByName("debug") {
      keyAlias = "debugAlias"
      keyPassword = "Qweqwe!23"
      storeFile = file("../keystore/debug.keystore")
      storePassword = "Qweqwe!23"
    }
  }

  defaultConfig {
    applicationId = "com.example.petcicd"
    minSdk = 26
    targetSdk = 32
    versionCode = 1
    versionName = "1.0"

    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

    buildConfigField("String", "country", "\"US\"")
    buildConfigField("String", "environment", "\"DEV\"")
  }

  buildTypes {
    release {
      applicationIdSuffix = ".release"
      versionNameSuffix = "-release"
      isMinifyEnabled = true
      // signingConfig = signingConfigs.getByName("release")
      // proguardFiles = getDefaultProguardFile("proguard-android-optimize.txt")
      resValue("string", "app_name", "cicd-release")

      configure<AppDistributionExtension> {
        artifactType = "APK"
        releaseNotes = "release notes"
        serviceCredentialsFile = "../petforcicd-7da81b19fb47.json"
        testers = "moloshniyV@gmail.com"
      }
    }
    debug {
      applicationIdSuffix = ".debug"
      versionNameSuffix = "-debug"
      signingConfig = signingConfigs.getByName("debug")
      resValue("string", "app_name", "cicd-debug")

      configure<AppDistributionExtension> {
        artifactType = "APK"
        releaseNotes = "release notes"
        serviceCredentialsFile = "./petforcicd-7da81b19fb47.json"
        testers = "moloshniyV@gmail.com"
      }
    }
  }
  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = "1.8"
  }
}

dependencies {
  implementation("com.google.dagger:hilt-android:2.41")
  kapt("com.google.dagger:hilt-android-compiler:2.41")
  implementation("androidx.core:core-ktx:1.7.0")
  implementation("androidx.appcompat:appcompat:1.4.2")
  implementation("com.google.android.material:material:1.6.1")
  implementation("androidx.constraintlayout:constraintlayout:2.1.4")
  testImplementation("junit:junit:4.13.2")
  androidTestImplementation("androidx.test.ext:junit:1.1.3")
  androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
  implementation(platform("com.google.firebase:firebase-bom:30.1.0"))
  implementation("com.google.firebase:firebase-analytics-ktx")
}

kapt {
  correctErrorTypes = true
}

configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {}

ktlint {
  android.set(true)
  outputColorName.set("RED")
}
